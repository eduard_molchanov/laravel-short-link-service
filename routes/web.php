<?php


Route::get("/", "ShortLinkController@index")->name("link");

Route::post("/", "ShortLinkController@store")->name("link.post");

Route::get("/{code}", "ShortLinkController@shortLink")->name("short.link");
