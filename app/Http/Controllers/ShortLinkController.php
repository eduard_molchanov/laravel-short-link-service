<?php

namespace App\Http\Controllers;

use App\ShortLink;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class ShortLinkController extends Controller
{
    public function index()
    {
        $links = ShortLink::orderBy("id", "desc")->get();

        return view("shortLink", compact("links"));
    }

    public function store(Request $request)
    {

        $request->validate([
            "link" => "required|url"
        ]);
        ShortLink::create([
            "link" => $request->link,
            "code" => str_random(6),
            "ip" => request()->server('SERVER_ADDR')
        ]);
        return redirect()->route("link")->with('success', 'Короткая ссылка успешно создана!)');
    }

    public function shortLink($code)
    {
        $link = ShortLink::where("code", $code)->first();

        return redirect($link->link);
    }
}
