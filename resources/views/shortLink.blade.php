<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

    <!-- Styles -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/uikit@3.5.14/dist/css/uikit.min.css"/>
    <style>
        html, body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Nunito', sans-serif;
            font-weight: 200;
            height: 100vh;
            margin: 0;
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }

        .content {
            text-align: center;
        }

        .title {
            font-size: 84px;
        }

        .links > a {
            color: #636b6f;
            padding: 0 25px;
            font-size: 13px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }

        .m-b-md {
            margin-bottom: 30px;
        }
    </style>
</head>
<body>
<div class="flex-center position-ref full-height">
    @if (Route::has('login'))
        <div class="top-right links">
            @auth
                <a href="{{ url('/home') }}">Home</a>
            @else
                <a href="{{ route('login') }}">Login</a>

                @if (Route::has('register'))
                    <a href="{{ route('register') }}">Register</a>
                @endif
            @endauth
        </div>
    @endif

    <div class="uk-container uk-container-large">
        <div class="title m-b-md">
            Short Link
        </div>

        <div class="">
            <form method="post" action="{{route("link.post")}}">
                @csrf
                <fieldset class="uk-fieldset">

                    <legend class="uk-legend">Сервис по созданию коротких ссылок на Laravel</legend>

                    <div class="uk-margin">
                        <input class="uk-input" type="text" placeholder="вставте ссылку" name="link">
                    </div>

                    <p uk-margin>

                        <button class="uk-button uk-button-primary" type="submit">Создать короткую ссылку</button>

                    </p>


                </fieldset>
            </form>

            @if(Session::has("success"))

                <div class="uk-alert-success" uk-alert>
                    <a class="uk-alert-close" uk-close></a>
                    <p>{{Session::get('success')}}</p>
                </div>
            @endif

            <table class="uk-table uk-table-striped">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Ваш IP</th>
                    <th>Сокращенная ссылка</th>
                    <th>Ссылка</th>
                </tr>
                </thead>
                <tbody>
                @isset($links)

                    @foreach($links as $link)
                        <tr>
                            <td>{{$link->id}}</td>
                            <td>{{$link->ip}}</td>
                            <td><a href="{{route("short.link", $link->code) }}" target="_blank">
                                    {{route("short.link", $link->code) }}
                                </a>
                            </td>
                            <td>{{$link->link}}</td>
                        </tr>
                    @endforeach
                @endisset
                </tbody>
            </table>

        </div>

    </div>
</div>
<!-- UIkit JS -->
<script src="https://cdn.jsdelivr.net/npm/uikit@3.5.14/dist/js/uikit.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/uikit@3.5.14/dist/js/uikit-icons.min.js"></script>
</body>
</html>
